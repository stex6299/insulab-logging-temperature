#!/usr/bin/env python3

import serial
import time

# Serial port parameter
serial_port = "/dev/ttyUSB1"
#serial_port = 'COM7'  
baud_rate = 9600  

# Initialize serial port
ser = serial.Serial(serial_port, baud_rate)

# Nome del file di log
log_file_name = 'temperature_log.txt'

try:
    while True:
        if ser.in_waiting > 0:
            
            # Read a line from serial port
            line = ser.readline().decode('utf-8').rstrip()
            
            # Current time
            current_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            
            # Line to be written
            log_entry = f"{current_time} - {line}\n"
            
            # Open file in append mode
            with open(log_file_name, 'a') as log_file:

                # Write line in output file
                log_file.write(log_entry)
                
                # Print in terminal
                print(log_entry)
                
                # Ensure data is written
                log_file.flush()
            
        # Wait 1 sec before next check
        time.sleep(1)

except KeyboardInterrupt:
    print("Bye bye")

finally:
    # Chiudi la porta seriale
    ser.close()
