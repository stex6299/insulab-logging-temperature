#!/usr/bin/env python3

import numpy as np
from matplotlib import pyplot as plt
import sys, os, re, glob

from datetime import datetime

infile = "temperature_log.txt"

# with open(infile, "r") as f:
#     temperatures = np.array([float(line.split(" ")[4]) for line in f.readlines() ])
#     # times = np.array([float(line.split(" - ")[0]) for line in f.readlines() ])
# with open(infile, "r") as f:
#     times = [datetime.strptime(line.split(" - ")[0], "%Y-%m-%d %H:%M:%S") for line in f ]
# with open(infile, "r") as f:
#     timesUnix = [datetime.strptime(line.split(" - ")[0], "%Y-%m-%d %H:%M:%S").timestamp() for line in f ]
    
temperatures = []
times = []
timesUnix = []

with open(infile, "r") as f:
    for line in f.readlines():
        temperatures.append(float(line.split(" ")[4]))

        myTime = datetime.strptime(line.split(" - ")[0], "%Y-%m-%d %H:%M:%S")
        
        times.append(myTime)
        timesUnix.append(myTime.timestamp())
        
        
temperatures = np.array(temperatures)
timesUnix = np.array(timesUnix)
    
# print(temperatures)
# print(times)




fig, ax = plt.subplots()
fig.set_size_inches(12,5)

ax.plot(times, temperatures , c = "tab:green", ls = "", marker = ".")

ax.set_title("Scatter plot", fontsize = 16)
ax.set_xlabel("Time a.u.", fontsize = 14)
ax.set_ylabel("Temperature °C", fontsize = 14)
ax.grid()
# ax.legend(fontsize = 14)

plt.show()





fig, ax = plt.subplots()
fig.set_size_inches(12,5)

h = ax.hist2d(timesUnix, temperatures , bins = 30, cmap = "jet")

fig.colorbar(h[3], ax = ax)

ax.set_title("Histogram 2d", fontsize = 16)
ax.set_xlabel("Time a.u.", fontsize = 14)
ax.set_ylabel("Temperature °C", fontsize = 14)
ax.grid()
# ax.legend(fontsize = 14)

plt.show()