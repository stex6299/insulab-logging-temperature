
const int tempPin = A0;
const float voltsPerDegree = 0.02; // change to 0.01 for TMP35 & 36

const int sleepmsec = 10000;


// Variables to store read and calculated values
int tempValue;
float volts;
float celsius;

void setup() {
  // Initialize serial 
  Serial.begin(9600);
}

void loop() {
  // Read value from analog pin
  tempValue = analogRead(tempPin);

  // Compute quantities
  volts = tempValue * 5.0 / 1024.0;
  celsius = (tempValue * 5.0 / 1024.0) / voltsPerDegree;

  // Write serial output
  Serial.print("Temperatura: ");
  Serial.print(celsius);
  Serial.println(" C");
  
  // Wait 10s
  delay(sleepmsec);
}
